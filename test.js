const testFolder = '../Tests/';
const fs = require('fs');

//short version

fs.readdir(testFolder, (error, files) => {
  let couter = 0;
    if(error !== null) {
        console.log(error);
      }
    files.filter(file => file.match('.*.txt')).forEach((fileName, index) => 
    {files[index] = fileName.replace(/\d/g, '');
    couter = couter + files[index].length});  
    console.log(couter);
});

//long version
fs.readdir(testFolder, (error, files) => {
  let couter = 0;
  if(error !== null) {
      console.log(error);
  }
  //Get full list of files in the current directory
  let fileList = files;
    
  //Filter out only the .txt files
  let txtFileList = fileList.filter(file => file.match('.*.txt'));
    
  //Remove all numbers from file names
  txtFileList.forEach((fileName, index) => txtFileList[index] = fileName.replace(/\d/g, ''));
    
  //Count the sum of lengths of all remaining file names
    
  txtFileList.forEach(fileName => couter = couter + fileName.length);
  console.log(couter);
});
